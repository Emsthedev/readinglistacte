

/* 1. Create a readingListActE folder. Inside create an index.js file.
2. Install nodemon and perform npm init command in gitbash to create a package.json file.
3. Install express and mongoose. Make sure to check these dependencies insde the package.json file.
4. Create a .gitignore file and store the node_modules in it.
5. Once done with your solution, create a repo named "readingListActE" and push your solution.
6. Save the repo link on S35-C1: Express.js: Data Persistende via Mongoos ODM */

/*

Activity Instructions:
1. Create a User schema with the following fields: firstName, lastName, username, password, email.
2. Create a Product schema with the following fields: name, description, price.
3. Create a User Model and  a Product Model.


Note: create a separate Database collection named S35-C1 in your MongoDb account.
*/
const express = require('express');
const mongoose = require("mongoose");

const app = express();
const port = 3002;
mongoose.connect(`mongodb+srv://root:root@zuitt-b197pt.xjoh3wx.mongodb.net/S35-c1?retryWrites=true&w=majority`,{
   useNewUrlParser : true, 
   useUnifiedTopology : true
});

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Successful Connection to MongoDB!'));


//Creating of Schema

const userSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    username: String, 
    password: String,
    email: String


})

const productSchema = new mongoose.Schema({
    name : String,
    description : String,
    price : Number
})

//Models

const User = mongoose.model('User', userSchema)
const Product = mongoose.model('Product', productSchema)

//Middleware

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Creation of Routes

/* 
4. Create a POST request that will access the /register route 
which will create a user. Test this in Postman app.
5. Create a POST request that will access the /createProduct route 
which will create a product. Test this in Postman app.
6. Create a GET request that will access the /users route 
to retrieve all users from your DB. Test this in Postman.
7. Create a GET request that will access the /products route 
to retrieve all products from your DB. Test this in Postman. */


app.post('/register',(req,res)=> {

    User.findOne({username: req.body.username}, (error, result) => {
        if(error){
            return res.send(error)
        } else if(result != null && result.username == req.body.username){
            return res.send('Username already taken!')
        } else{
            let newUser = new User({
                username: req.body.username,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                password: req.body.password,
                email: req.body.email

            })

            newUser.save((error, savedUser) => {
                if(error) {
                    return console.error(error)
                }else {
                    return res.status(201).send('New User Created')
                }
            })
        }
    })
})


//Retrive all Users
app.get('/users', (req,res) => {
    User.find({}, (error, result) => {
        if(error){
            return res.send(error)
        } else{
            return res.status(200).json({
                users : result
            })
        }
    })
})

//CREATE Product

app.post('/createProduct',(req,res)=> {

    Product.findOne({name: req.body.name}, (error, result) => {
        if(error){
            return res.send(error)
        } else if(result != null && result.name == req.body.name){
            return res.send('Product name is already taken!')
        } else{
            let newProduct = new Product({
                name: req.body.name,
                description : req.body.description,
                price : req.body.price
                
            })

            newProduct.save((error, savedProduct) => {
                if(error) {
                    return console.error(error)
                }else {
                    return res.status(201).send('New Product Created')
                }
            })
        }
    })
})

//Retrive All Users
app.get('/products', (req,res) => {
    Product.find({}, (error, result) => {
        if(error){
            return res.send(error)
        } else{
            return res.status(200).json({
                products : result
            })
        }
    })
})






app.listen(port, () => console.log(`Server is running at port ${port}`))

